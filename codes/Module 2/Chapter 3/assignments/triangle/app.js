var t1 = document.getElementById("t1");
var t2 = document.getElementById("t2");
var t3 = document.getElementById("t3");
function f1() {
    var a = parseFloat(t1.value);
    var b = parseFloat(t2.value);
    var c = parseFloat(t3.value);
    if (a != b && b != c && a != c) {
        document.getElementById("ies").innerHTML = "Scalene Triangle";
    }
    else if (a == b && b == c && a == c) {
        document.getElementById("ies").innerHTML = "Equilateral Triangle";
    }
    else {
        document.getElementById("ies").innerHTML = "Isosceles Triangle";
    }
    var h = Math.max(a, Math.max(b, c));
    if ((a * a == b * b + c * c) || (b * b == a * a + c * c) || (c * c == a * a + b * b)) {
        document.getElementById("rat").innerHTML = "Right Angled Triangle";
    }
}
//# sourceMappingURL=app.js.map