var t1 = document.getElementById("t1");
var t2 = document.getElementById("t2");
var t3 = document.getElementById("t3");
var t4 = document.getElementById("t4");
var t5 = document.getElementById("t5");
var t55 = document.getElementById("t55");
var t6 = document.getElementById("t6");
var t66 = document.getElementById("t66");
var t7 = document.getElementById("t7");
var t8 = document.getElementById("t8");
var t88 = document.getElementById("t88");
var t9 = document.getElementById("t9");
function f1() {
    var c = (t1.value).length;
    t2.value = c.toString();
}
function f2() {
    var c = (t1.value).toUpperCase();
    t2.value = c.toString();
}
function f3() {
    var c = (t1.value).toLowerCase();
    t2.value = c.toString();
}
function f4() {
    var c = (t1.value).trim();
    t2.value = c.toString();
}
function f5() {
    var c = (t1.value).indexOf(t3.value);
    t2.value = c.toString();
}
function f6() {
    var c = (t1.value).lastIndexOf(t4.value);
    t2.value = c.toString();
}
function f7() {
    var c = (t1.value).substring(parseInt(t5.value), parseInt(t55.value));
    t2.value = c.toString();
}
function f8() {
    var c = (t1.value).replace((t6.value), (t66.value));
    t2.value = c.toString();
}
function f9() {
    var c = (t1.value).concat(t7.value);
    t2.value = c.toString();
}
function f10() {
    var c = (t1.value).slice(parseInt(t5.value), parseInt(t55.value));
    t2.value = c.toString();
}
function f11() {
    var c = (t1.value).charAt(parseInt(t9.value));
    t2.value = c.toString();
}
function f12() {
    var c = (t1.value);
    var arr = c.split(" ");
    for (var i in arr) {
        alert("Element at position " + i + " is " + arr[i]);
    }
    t2.value = arr.toString();
}
//# sourceMappingURL=app.js.map