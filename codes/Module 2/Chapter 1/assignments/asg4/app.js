var t1 = document.getElementById("t1");
var t2 = document.getElementById("t2");
var t3 = document.getElementById("t3");
var t4 = document.getElementById("t4");
var t5 = document.getElementById("t5");
var x = document.getElementById("base");
x.style.display = "none";
var y = document.getElementById("exp");
y.style.display = "none";
function f1() {
    try {
        var c = parseFloat(t1.value) + parseFloat(t2.value);
    }
    catch (e) {
        alert("Error: " + e.description);
    }
    t3.value = c.toString();
}
function f2() {
    var c = parseFloat(t1.value) - parseFloat(t2.value);
    t3.value = c.toString();
}
function f3() {
    var c = parseFloat(t1.value) * parseFloat(t2.value);
    t3.value = c.toString();
}
function f4() {
    try {
        var c = parseFloat(t1.value) / parseFloat(t2.value);
    }
    catch (e) {
        alert("Error: " + e.description);
    }
    t3.value = c.toString();
}
function f5() {
    var c = Math.pow(parseInt(t1.value), parseInt(t2.value));
    t3.value = c.toString();
}
function f6() {
    var c = Math.sin(parseInt(t1.value) * Math.PI / 180);
    t3.value = c.toString();
}
function f7() {
    var c = Math.cos(parseInt(t1.value) * Math.PI / 180);
    t3.value = c.toString();
}
function f8() {
    var c = Math.tan(parseInt(t1.value) * Math.PI / 180);
    t3.value = c.toString();
}
function eq() {
    try {
        var c = eval(t4.value);
    }
    catch (e) {
        alert("Error: " + e.description);
        t5.value = e.description;
    }
    finally {
        t5.value = c.toString();
    }
}
function a1() {
    x.style.display = "block";
    y.style.display = "none";
}
function a2() {
    y.style.display = "block";
    x.style.display = "none";
}
//# sourceMappingURL=app.js.map